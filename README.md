# Smart traffic light system

This project uses two Arduino Uno&#39;s which remotely connect and share sensor data via Bluetooth with a C# console app.

The C# App processes the data in real-time and gives directions to the robot car. The directions depend on it&#39;s location in comparison to the smart traffic light and what signal the traffic light is being used currently.

The signal is affected by the App informing it of the presence of the Robot car nearby, so it turns green and allows the car to pass.

The App created acts as an API which simulates the Fog computing ideal architecture in order to prove that at least at a basic level it can work.