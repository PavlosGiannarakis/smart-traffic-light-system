﻿using System;
using System.IO.Ports;
using System.Threading;

namespace Fog
{
    class Program
    {
        //Serial Port connections via Bluetooth
        static SerialPort BlueToothConnection;
        static SerialPort BlueToothConnection2;
        /*
         * Boolean flags for Demo, ensure communication between the two threads
         * while also ensuring the setup was successful.
         */
        static bool stopped = false;
        static bool light = false;
        static bool complete = false;
        static bool setup_success = true;

        //Method for managing the car based on its data input and the flags set above.
        public static void HandleCar(SerialPort x)
        {
            //Stores previous position but is not used extensively.
            int p = -1;
            
            //Enter infinite loop until broken.
            while (true)
            {
                try
                {
                    //Get Data
                    var c = x.ReadLine();

                    try
                    {
                        //Convert data to integer
                        var i = Convert.ToInt32(c);

                        // Check data and conditions and issue commands accordingly.

                        if (stopped && light)
                        {
                            x.Write("f");
                            complete = true;
                            break;
                        }

                        if (p == -1 && i > 4)
                        {
                            Console.WriteLine("The car started moving.");
                            stopped = false;
                            x.Write("b");
                        }
                        else if (i > 4)
                        {
                            stopped = false;
                            x.Write("b");
                        }
                        else if (i < 4 && !stopped)
                        {
                            Console.WriteLine("The car has stopped.");
                            stopped = true;
                            x.Write("s");
                            Thread.Sleep(3000);
                        }
                        p = i;
                    }
                    catch { }
                }
                catch { }
            }
        }

        //Method to handle traffic light.
        public static void HandleTrafficLight(SerialPort x)
        {
            //Stores previous sensor data.
            int p = -1;

            //Infinite loop
            while (true)
            {
                try
                {
                    //Get Data
                    var c = x.ReadLine();
                    try
                    {
                        //Convert data to integer
                        var i = Convert.ToInt32(c);
                        //Check for conditions and issue commands accordingly.
                        if (complete)
                        {
                            Console.WriteLine("End of Demo.");
                            break;
                        }
                        if (p == -1)
                        {
                            if (i < 10)
                            {
                                Console.WriteLine("The lights are Green.");
                                BlueToothConnection2.Write("g");
                                light = true;
                            }
                            else
                            {
                                Console.WriteLine("The lights are Red.");
                            }
                        }
                        else if (i < 10 && p >= 10)
                        {
                            Console.WriteLine("The lights are Green.");
                            BlueToothConnection2.Write("g");
                            light = true;
                        }
                        else if (i >= 10 && p < 10)
                        {
                            Console.WriteLine("The lights are Red.");
                            BlueToothConnection2.Write("r");
                            light = false;
                        }
                        p = i;
                    }
                    catch { }
                }
                catch { }
            }
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine("Starting Application.");

            //Attempt to establish bluetooth connection with set parameters, if not possible then set up is unsuccessful.
            BlueToothConnection = new SerialPort();
            BlueToothConnection.BaudRate = 9600;
            BlueToothConnection.PortName = "COM4";
            BlueToothConnection.ReadTimeout = 500;
            //Catch any error thrown.
            try {
                BlueToothConnection.Open();
                Thread.Sleep(200);
                Console.WriteLine("Connected to Robot Car.");
            } catch {
                Thread.Sleep(200);
                Console.WriteLine("Not Connected to Robot Car.");
                //If any error is caught then set flag to false to prevent the application from continuing.
                setup_success = false;
            }

            //Replicate the above process for a second serial port.
            BlueToothConnection2 = new SerialPort();
            BlueToothConnection2.BaudRate = 9600;
            BlueToothConnection2.PortName = "COM9";
            BlueToothConnection2.ReadTimeout = 500;
            try
            {
                BlueToothConnection2.Open();
                Thread.Sleep(200);
                Console.WriteLine("Connected to Traffic lights.");
            }
            catch {

                Thread.Sleep(200);
                Console.WriteLine("Not Connected to Traffic lights.");
                setup_success = false;
            }

            //Test set up status and output text to console accordingly.
            if (setup_success)
            {
                Console.WriteLine("Set up successful.");

                Console.WriteLine("Press any button to start demo.");
                Console.ReadKey();
                Thread car = new Thread(() => HandleCar(BlueToothConnection));
                Thread traffic_light = new Thread(() => HandleTrafficLight(BlueToothConnection2));
                try {
                    car.Start();
                    traffic_light.Start();
                } catch { }
                
            }
            else {
                Console.WriteLine("Unable to establish a connection.");
            }

            //Wait for command to close.
            Console.ReadKey();
        }
    }
}
