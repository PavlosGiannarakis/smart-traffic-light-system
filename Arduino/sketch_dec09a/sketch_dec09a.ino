//#include <SoftwareSerial.h>
//SoftwareSerial ms(6,7);
int led1 = 12;
int led2 = 11;
char message = ' ';
const int trigPin1 = 4;
const int echoPin1 = 3;
// defines variables
long duration1;
int distance1;
char c = ' ';

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  //ms.begin(38400);
  pinMode(trigPin1, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin1, INPUT); // Sets the echoPin as an Input
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  rled_on();
}

void loop() {
  firstsensor();
  while(Serial.available() > 0)
  {
    message = Serial.read();//store string from serial command
    //Serial.println(message);
  }
  if(message == 'g')
  {
    rled_off();
    gled_on();
    message = ' ';
  }
  else if (message == 'r')
  {
    gled_off();
    rled_on();
    message = ' ';
  }
  delay(100);
}
void firstsensor(){ // This function is for first sensor.
  digitalWrite (trigPin1, HIGH);
  delayMicroseconds (10);
  digitalWrite (trigPin1, LOW);
  duration1 = pulseIn (echoPin1, HIGH);
  distance1 = (duration1/2) / 29.1;
      Serial.println(distance1);
}
void rled_on(){
  digitalWrite(led1, HIGH);
}
 
void rled_off(){
  digitalWrite(led1, LOW);
}

void gled_on(){
  digitalWrite(led2, HIGH);
}
 
void gled_off(){
  digitalWrite(led2, LOW);
}
