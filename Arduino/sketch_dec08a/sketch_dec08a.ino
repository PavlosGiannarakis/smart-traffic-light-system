#include <SoftwareSerial.h>
SoftwareSerial ms(5,6);

int motor_left[] = {3, 2};
int motor_right[] = {8, 9};
int led = 13;
char message = ' ';
const int trigPin1 = 11;
const int echoPin1 = 10;
// defines variables
long duration1;
int distance1;
 
void setup() {
  Serial.begin(9600); //set baud rate
  Serial.println("Hello1");
  
  ms.begin(9600);
  //ms.println("Hello2");
  
  int i;
  pinMode(trigPin1, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin1, INPUT); // Sets the echoPin as an Input
  for(i = 0; i < 2; i++){
    pinMode(motor_left[i], OUTPUT);
    pinMode(motor_right[i], OUTPUT);
    pinMode(led, OUTPUT);
  }
  //Serial.println("There is life in this Arduino.");
}
 
void loop() {
  led_on();
  while(ms.available() > 0)
  {
    message = ms.read();//store string from serial command
    //ms.println(message);
  }
  if(message == ' ')
  {
      firstsensor();
      delay(100);
  }
  //if(!ms.available())
  //{
    if(message=='f')
    {
       drive_forward(1000);
       motor_stop(25);
       message=' '; //clear the data
       
      //Serial.println("Here F");
    }
    else if(message=='b')
    {
      drive_backward(100);
      motor_stop(25);
      message=' '; //clear the data
      //Serial.println("Here R");
    }
    else if(message=='r')
    {
      turn_right(1000);
      motor_stop(25);
      message=' '; //clear the data
     // Serial.println("Here R");
    }
    else if(message=='l')
    {
      turn_left(1000);
      motor_stop(25);
      message=' '; //clear the data
      //Serial.println("Here L");
    }
    else if(message=='s')
    {
      //turn_left(1000);
      motor_stop(25);
      message=' '; //clear the data
      //Serial.println("Here L");
    }
    //message = ' '; //clear the data
  //}
 }

void firstsensor(){ // This function is for first sensor.
  digitalWrite (trigPin1, HIGH);
  delayMicroseconds (10);
  digitalWrite (trigPin1, LOW);
  duration1 = pulseIn (echoPin1, HIGH);
  distance1 = (duration1/2) / 29.1;
      ms.println(distance1);
      //Serial.println(distance1);
}
 
 
void led_on(){
  digitalWrite(led, HIGH);
}
 
void led_off(){
  digitalWrite(led, LOW);
}
 
void motor_stop(int duration){
  digitalWrite(motor_left[0], LOW); 
  digitalWrite(motor_left[1], LOW); 
   
  digitalWrite(motor_right[0], LOW); 
  digitalWrite(motor_right[1], LOW);
 
  delay(duration);
}
 
void drive_forward(int duration){
  digitalWrite(motor_left[0], HIGH); 
  digitalWrite(motor_left[1], LOW); 
   
  digitalWrite(motor_right[0], HIGH); 
  digitalWrite(motor_right[1], LOW); 
   
  delay(duration);
}
 
void drive_backward(int duration){
  digitalWrite(motor_left[0], LOW); 
  digitalWrite(motor_left[1], HIGH); 
   
  digitalWrite(motor_right[0], LOW); 
  digitalWrite(motor_right[1], HIGH); 
   
  delay(duration);
}
 
void turn_left(int duration){
  digitalWrite(motor_left[0], LOW); 
  digitalWrite(motor_left[1], HIGH); 
   
  digitalWrite(motor_right[0], HIGH); 
  digitalWrite(motor_right[1], LOW);
   
  delay(duration);
}
 
void turn_right(int duration){
  digitalWrite(motor_left[0], HIGH); 
  digitalWrite(motor_left[1], LOW); 
   
  digitalWrite(motor_right[0], LOW); 
  digitalWrite(motor_right[1], HIGH); 
   
  delay(duration);
}
